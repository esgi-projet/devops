FROM node:12.18.1
LABEL maintainer="rkouache@myges.fr"

ADD . /app/
WORKDIR /app

RUN npm install --force

EXPOSE 5000
